import sys
sys.path.append('../../TINYpub/TINY/')
import TINY 
import UTILS
from mes_imports import *
import load_data_Loader
import SOLVE_EB as EB


class MaxPoolSELU(torch.nn.Module) :
    def __init__(self):
        super(MaxPoolSELU, self).__init__()
        self.kernel_size = 2
    def forward(self, x) :
        return(torch.nn.SELU()(torch.nn.MaxPool2d(kernel_size=self.kernel_size)(x)))
    


from GLOBALS_CIFAR10 import *
import os
os.mkdir(name_file_expe)


### Loss function ###
def Loss_entropy(x1, x2, reduction = 'mean') :
    #print(x1.shape, x2.shape)
    return(torch.nn.CrossEntropyLoss(reduction = reduction)(x1, x2))




dico_parameters = {
                 'skeleton' : copy.deepcopy(skeleton),
                'Loss' : Loss_entropy,
                'fct' : fct,
                'layer_name' :layer_name,
                'init_deplacement' : 1e-8, # min amplitude factor
                'batch_size' : 64,# batchsize for training
                'lr' : 1e-2,# leraning rate for training
                'lambda_method' : 0, # = 0 for searching the amplitude factor, if > 0 the 
                                     # amplitude factor is automatically set to this value
                'accroissement_decay' : 1e-4,# the minimum decay to update the 
                                             # architecture with the NewNeurons/BestUpdate 
                'depth_seuil' : depth_seuil,# maximum number of neurons to add per layer
                'lu_lin' : 2,
                'max_batch_estimation' : 5000,
                'gradient_clip' : 1e-3,
                'max_amplitude' : 2., # max amplitude factor
                'ind_lmbda_shape' : 50000,
                'init_X_shape' : [3, 32, 32], # size of the input, if your X are 1-d, unsqueeze them
                'len_train_dataset' : 50000, # size of training data
                'len_test_dataset' : 10000, # size of testing data
                'selection_neuron' : UTILS.selection_neuron_seuil,
                'how_to_define_batchsize' : UTILS.indices_non_constant,
                }


MLP_model = TINY.TINY(dico_parameters)
MLP_model.training_data, MLP_model.test_data = load_data_Loader.load_database_CIFAR10(root='../../TINYpub/DEMO/CL/data', AugD=False)
MLP_model.tr_loader, MLP_model.te_loader =  iter(UTILS.cycle(DataLoader(MLP_model.training_data, batch_size=50000, shuffle=False))),  iter(UTILS.cycle(DataLoader(MLP_model.test_data, batch_size=10000, shuffle=False)))


TINY.X_train_rescale, TINY.Y_train_rescale = next(MLP_model.tr_loader)
TINY.X_test_rescale, TINY.Y_test_rescale = next(MLP_model.te_loader)

del MLP_model.tr_loader, MLP_model.te_loader


df_tracker = pd.DataFrame()
A_tr, A_te, L_tr, L_te, T = np.array([]), np.array([]), np.array([]), np.array([]), np.array([0])

nbr_pass = 20
#nbr_epochs_betw_adding = 0.5 ## Epochs of training between adding
nbr_steps_betw_adding = 0






def update_quantity_of_interest(method = 'Add', depth = 1) :
    global L_tr, L_te, A_tr, A_te, T, df_tracker
    dico_DV = {'DV_norm_' + str(d) : 0. for d in range(1, MLP_model.deep + 1)}
    indx = torch.randperm(MLP_model.len_train_dataset)[:size_batch_estimation]
    for sous_indices in range(math.ceil(indx.shape[0]/MLP_model.max_batch_estimation)) :
        sous_ind = indx[sous_indices * MLP_model.max_batch_estimation : (sous_indices + 1) * MLP_model.max_batch_estimation]
        sous_seed = MLP_model.seed_lmbda[sous_indices * MLP_model.max_batch_estimation : (sous_indices + 1) * MLP_model.max_batch_estimation]
        X, Y = MLP_model.get_batch(indices = sous_ind, seed = sous_seed[0], device = my_device_0)
        for d in range(1, MLP_model.deep + 1):
            dico_DV['DV_norm_' + str(d)] += (MLP_model.deplacement_voulu(d, X=X, Y = Y).norm()**2 / X.shape[0]).item()
    
    for d in range(1, MLP_model.deep + 1):
            dico_DV['DV_norm_' + str(d)] = np.sqrt(dico_DV['DV_norm_' + str(d)])
    
    if method == 'Add' :
        #DV = MLP_model.deplacement_voulu(depth, X=X, Y = Y)
        dico_tracker = {'vps' + str(i) : [ MLP_model.valeurs_propres[i].item()] for i in range(len(MLP_model.valeurs_propres))}
        dico_tracker.update({'depth_add' : depth, 'nbr_added_neuron': MLP_model.nbr_added_neuron,
                        'sum_vps_sq' : (MLP_model.valeurs_propres ** 2).sum().item(),
                        'count': count, 'DV_proj_norm_sq': MLP_model.DV_proj_norm_sq.item()})

    elif method == 'NG' : 
        #DV = MLP_model.deplacement_voulu(depth + 1, X=X, Y = Y)
        dico_tracker = {}
        dico_tracker.update({'depth_NG' : depth, 'nbr_added_neuron': MLP_model.nbr_added_neuron,
                        'count': count})
    else:
        assert False
        
    dico_tracker.update({
        #'accroissement' : [dico_EB[best_depth]['accroissement']], 'portion_gain' : [dico_EB[best_depth]['portion_gain']],
                        'nbr_parameters_apres' : [MLP_model.count_parameters()], 'nbr_parameters_avant' : [nbr_parameters_avant],
                        'T' : [T[-1]], 'len_L_tr' : [len(L_tr)], 'lu_conv' : [MLP_model.lu_conv]})
    
    dico_tracker.update({
                    'size_layer_' + str(i) : [MLP_model.skeleton[i].get('out_channels', MLP_model.skeleton[i].get('size', 0))] for i in range(1, MLP_model.deep + 1)
                       })
   
    
    dico_tracker.update(dico_DV)
    df_tracker = pd.concat([df_tracker, pd.DataFrame.from_dict(dico_tracker)], ignore_index=True)
    
    L_tr = np.concatenate([L_tr, l_tr.unsqueeze(0)])
    L_te = np.concatenate([L_te, l_te.unsqueeze(0)])
    A_tr = np.concatenate([A_tr, a_tr.unsqueeze(0)])
    A_te = np.concatenate([A_te, a_te.unsqueeze(0)])
    T = np.concatenate([T, t.unsqueeze(0) +  T[-1]])

    
    
    
    
def AugBatchSizeLearning() :
    MLP_model.batch_size = math.ceil(np.sqrt(MLP_model.count_parameters() / nbr_parameters_avant) *  MLP_model.batch_size)
    #MLP_model.batch_size = math.ceil(MLP_model.count_parameters() / nbr_parameters_avant *  MLP_model.batch_size)



def compute_Loss_full(model):
    """
    Computes the avearged and individual losses of the network for a minibatch 
    either when adding the BestUpdate or the Newneurons, 
    with the amplitude factor 'ampl_fact'.
    """
    #indx = torch.arange(model.len_train_dataset)
    indx = torch.randperm(MLP_model.len_train_dataset)[:size_batch_estimation]
    L_tr = torch.tensor(0., device = my_device)
    A_tr = torch.tensor(0., device = my_device)
    for sous_indices in range(math.ceil(indx.shape[0]/model.max_batch_estimation)) :
        sous_ind = indx[sous_indices * model.max_batch_estimation : (sous_indices + 1) * model.max_batch_estimation]
        sous_seed = model.seed_lmbda[sous_indices * model.max_batch_estimation : (sous_indices + 1) * model.max_batch_estimation]
        X, Y = model.get_batch(indices = sous_ind, seed = sous_seed[0], device = my_device_0)
        #l[sous_indices * model.max_batch_estimation : (sous_indices + 1) * model.max_batch_estimation] = model.Loss(fct_to_apply(X, depth = depth, lmbda = ampl_fact)[0], Y.to(my_device), reduction = 'none')
        with torch.no_grad() :
            Y_pred = model(X)
        L_tr += model.Loss(Y_pred, Y, reduction = 'sum') /  indx.shape[0]
        A_tr += UTILS.calculate_accuracy(Y_pred, Y) * sous_ind.shape[0] / indx.shape[0]

    #indx = torch.arange(model.len_test_dataset)
    indx = torch.randperm(MLP_model.len_test_dataset)[:size_batch_estimation]
    L_te = torch.tensor(0., device = my_device)
    A_te = torch.tensor(0., device = my_device)

    for sous_indices in range(math.ceil(indx.shape[0]/model.max_batch_estimation)) :
        sous_ind = indx[sous_indices * model.max_batch_estimation : (sous_indices + 1) * model.max_batch_estimation]
        sous_seed = model.seed_lmbda[sous_indices * model.max_batch_estimation : (sous_indices + 1) * model.max_batch_estimation]
        X, Y = model.get_batch(indices = sous_ind, seed = sous_seed[0], device = my_device_0, data = 'te')
        #l[sous_indices * model.max_batch_estimation : (sous_indices + 1) * model.max_batch_estimation] = model.Loss(fct_to_apply(X, depth = depth, lmbda = ampl_fact)[0], Y.to(my_device), reduction = 'none')
        with torch.no_grad() :
            Y_pred = model(X)
        L_te += model.Loss(Y_pred, Y, reduction = 'sum') /  indx.shape[0]
        A_te += UTILS.calculate_accuracy(Y_pred, Y) * sous_ind.shape[0] / indx.shape[0]
    return(L_tr.cpu(), L_te.cpu(), A_tr.cpu(), A_te.cpu()) 


def batch_size_estimation() :
    
    size_batch = size_batch_estimation
    size_batch = int(size_batch)

    size_batch = max(1000, size_batch)
    if MLP_model.force_small_estimation_batch or MLP_model.architecture_growth == 'Random' :
        size_batch = 100

    l = math.ceil(size_batch / MLP_model.max_batch_estimation) * MLP_model.max_batch_estimation

    if not (getattr(MLP_model, 'tr_loader', None) is None):
        max_len_l = max(MLP_model.len_train_dataset, l)
    else:
        max_len_l = l

    if not (getattr(MLP_model, 'tr_loader', None) is None):
        max_len_l = min(MLP_model.len_train_dataset, l)
    else:
        max_len_l = max(MLP_model.len_train_dataset, l)

    MLP_model.ind = torch.randperm(MLP_model.len_train_dataset)[:l]
    MLP_model.seed = torch.randperm(MLP_model.len_train_dataset)[:l]
    MLP_model.ind_lmbda = torch.randperm(MLP_model.len_train_dataset)[:MLP_model.ind_lmbda_shape]
    MLP_model.seed_lmbda = torch.randperm(MLP_model.len_train_dataset)[:MLP_model.ind_lmbda_shape]


    
    
count = 1




for j in tqdm(range(5)) :
    t_0 = time.time()
    for k in range(len(to_add)) :
        t_0 = time.time()
        nbr_parameters_avant = MLP_model.count_parameters()
        gc.collect()
        depth = to_add[k]
        MLP_model.dico_w = None
        batch_size_estimation()
        EB.compute_optimal_update(MLP_model, depth + 1, update = False, compute_gain=False)
        batch_size_estimation()
        EB.add_neurons(MLP_model, depth, update = True)
        
        l_tr, l_te, a_tr, a_te = compute_Loss_full(MLP_model)
        t = torch.tensor(time.time() - t_0, device = my_device)
        update_quantity_of_interest(method ='Add', depth = depth)
        for d in MLP_model.skeleton.keys() :
            t_0 = time.time()
            MLP_model.dico_w = None
            batch_size_estimation()
            nbr_parameters_avant = MLP_model.count_parameters()
            EB.compute_optimal_update(MLP_model, depth + 1, update = True)

            l_tr, l_te, a_tr, a_te = compute_Loss_full(MLP_model)
            t = torch.tensor(time.time() - t_0, device = my_device)
            update_quantity_of_interest(method = 'NG', depth = d)

            count += 1

        
    
        df_performance = pd.DataFrame.from_dict({'L_tr' : L_tr, 'L_te' : L_te, 'A_tr' : A_tr, 'A_te' : A_te, 'T' : T[1:], 
                                                 'BatchSize' : np.ones(A_te.shape) * MLP_model.batch_size})
        df_tracker.to_csv(name_file_expe + '/df_tracker.csv')
        df_performance.to_csv(name_file_expe + '/df_performance.csv')
        MLP_model.T = T[-1]
        MLP_model.len_L_tr = len(L_tr)
    UTILS.save_model_to_file(MLP_model, path = name_file_expe + '/' , name='model_' + str((j + 1) * (k + 1)))











