activity = {}
dico_mask_tensor_t = {}
dico_tt = {}
dico_F = {}
dico_T_permute_flat = {}
dico_T_flat_sparse = {}
outputs = {}


batch_size = 32
lr = 1e-2

init_deplacement = 1e-8
init_deplacement_NG = init_deplacement
force_update = False
max_amplitude = 5
do_NG = False
lu_conv = 0.001
TailleBatchFixe = False


verbose = False


def print_globals():
    if verbose:
        dictionaries = {
            "activity": activity,
            "dico_T": dico_mask_tensor_t,
            "dico_TT": dico_tt,
            "dico_F": dico_F,
            "dico_T_permute_flat": dico_T_permute_flat,
            "dico_T_flat_sparse": dico_T_flat_sparse,
            "outputs": outputs
        }

        for k, v in dictionaries.items():
            if v:
                print('=' * 20)
                print(f"Global variable: {k}")
                print('keys:', v.keys())
                assert isinstance(v, dict), f"Expected a dictionary, got {type(v)}"
                try:
                    print('shapes:', {key: val.shape if val is not None else None for key, val in v.items()})
                except AttributeError:
                    pass

        print('\n', '-' * 60, '\n')


def print_model_architecture(model: 'TINY'):
    print('depth  |  layer type | in_chan->out_chan / output size')
    for k in sorted(list(model.skeleton.keys()))[1:]:
        if model.layer_name[k][0] == 'C':
            print(f"depth {k} | Conv | {model.skeleton[k]['in_channels']} -> {model.skeleton[k]['out_channels']} (kernel size: {model.skeleton[k]['kernel_size']}, padding: {model.skeleton[k].get('padding')}, stride: {model.skeleton[k].get('stride')})")
            # print('depth ' + str(k) + ' |  Conv | ' + str(RN.outputs_size_after_activation[k]))
        else:
            print('depth ' + str(k) + ' | Linear | ' + str(model.skeleton[k]['size']))
            # print('depth ' + str(k) + ' | Linear | ' + str(RN.outputs_size_after_activation[k]))
        if k in model.fct.keys():
            print(f'fct_{k} := {model.fct[k]}')

        print(f"At detph {k}, the output size is {model.outputs_size_after_activation[k]}")

        if k in dico_mask_tensor_t and dico_mask_tensor_t[k] is not None:
            print(f"Tensor T at depth {k} has shape {dico_mask_tensor_t[k].shape}")
        if k in dico_tt and dico_tt[k] is not None:
            print(f"Tensor TT at depth {k} has shape {dico_tt[k].shape}")
        if k in dico_F and dico_F[k] is not None:
            print(f"Tensor F at depth {k} has shape {dico_F[k].shape}")

            
            
            








paths = ['results/GradMax/1soixantequatre_tbadd_1/1']

architecture_growth = 'GradMax'
nbr_epochs_betw_adding = 1
reduction = 64
rescale = 'theta'
nbr_pass = 21
lambda_method = 1e-3
AG = 'GradMax'


'''
paths = ['resultats/1quart_tbadd_1/3', 'resultats/1quart_tbadd_1/4']

architecture_growth = 'GradMax'
nbr_epochs_betw_adding = 1
reduction = 4
rescale = 'theta'
nbr_pass = 8
lambda_method = 1e-3
'''


'''
paths = ['resultats/1quart_tbadd_0.25/1', 'resultats/1quart_tbadd_0.25/2']
architecture_growth = 'Our'
nbr_epochs_betw_adding = 0.25
reduction = 4
rescale = 'DE'
nbr_pass = 8
lambda_method = 0
'''

'''
paths = ['resultats/1quart_tbadd_0.25/3', 'resultats/1quart_tbadd_0.25/4']

architecture_growth = 'GradMax'
nbr_epochs_betw_adding = 0.25
reduction = 4
rescale = 'theta'
nbr_pass = 8
lambda_method = 1e-3
'''

'''
paths = ['resultats/1soixantequatre_tbadd_0.25/1', 'resultats/1soixantequatre_tbadd_0.25/2']
architecture_growth = 'Our'
nbr_epochs_betw_adding = 0.25
reduction = 64
rescale = 'DE'
nbr_pass = 21
lambda_method = 0
'''
'''
paths = ['resultats/1soixantequatre_tbadd_0.25/3', 'resultats/1soixantequatre_tbadd_0.25/4']

architecture_growth = 'GradMax'
nbr_epochs_betw_adding = 0.25
reduction = 64
rescale = 'theta'
nbr_pass = 21
lambda_method = 1e-3
'''

'''

paths = ['resultats/1soixantequatre_tbadd_1/1', 'resultats/1soixantequatre_tbadd_1/2']
architecture_growth = 'Our'
nbr_epochs_betw_adding = 1
reduction = 64
rescale = 'DE'
nbr_pass = 21
lambda_method = 0

'''

'''
paths = ['resultats/1soixantequatre_tbadd_1/3', 'resultats/1soixantequatre_tbadd_1/4']
architecture_growth = 'Our'
nbr_epochs_betw_adding = 1
reduction = 64
rescale = 'theta'
nbr_pass = 21
lambda_method = 1e-3
'''
