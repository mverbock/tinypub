import sys
sys.path.append('../../TINYpub/TINY/')
import TINY 
#import TINY_AF_AddNG as TINY
#print('PACKAGE TINY_AF_AddNG !!!!!!!!!')
import UTILS
from mes_imports import *
import load_data_Loader
import SOLVE_EB as EB


# the type of layer
layer_name = {1 : 'L', 2 : 'L', 3 :'L'}

# the size of the init layers
skeleton = {0: {}, 1 : {'size' : 1}, 2 : {'size' : 1}, 3 : {'size' : 10}}

# the activation function for each layer
fct = {depth : torch.nn.SELU() for depth in range(1, 3)}
fct.update({3 : torch.nn.Identity()})

### Loss function ###
def Loss_entropy(x1, x2, reduction = 'mean') :
    #print(x1.shape, x2.shape)
    return(torch.nn.CrossEntropyLoss(reduction = reduction)(x1, x2))

## batch size ##
def define_Batchsize(model, depth, method = None) :
    model.ind=torch.arange(model.len_train_dataset)
    model.ind_lmbda=torch.arange(model.len_train_dataset)
    model.seed=torch.randperm(model.len_train_dataset)
    model.seed_lmbda = torch.randperm(model.len_train_dataset)
    



to_add = [1, 2] # depths where we want to add neurons
nbr_steps_betw_adding = 0 # number of gradient step between each architecture growth

dico_parameters = {
                 'skeleton' : copy.deepcopy(skeleton),
                'Loss' : Loss_entropy,
                'fct' : fct,
                'layer_name' :layer_name,
                'init_deplacement' : 1e-8, # min amplitude factor
                'batch_size' : 64,# batchsize for training
                'lr' : 1e-2,# learning rate for training
                'lambda_method' : 0, # = 0 for searching the amplitude factor, if > 0 the 
                                     # amplitude factor is automatically set to this value
                'accroissement_decay' : 1e-4,# the minimum decay to update the 
                                             # architecture with the NewNeurons/BestUpdate 
                'depth_seuil' : {1 : 1, 2 : 1},# maximum number of neurons to add per layer
                'max_batch_estimation' : 500, # computer batchsize
                'max_amplitude' : 2., # max amplitude factor
                'ind_lmbda_shape' : 500, # size of the batch to estimate the amplitude factor
                'init_X_shape' : [1, 1, 50], # size of the input, if your X are 1-d, unsqueeze them
                'len_train_dataset' : 500, # size of training data
                'len_test_dataset' : 50, # size of testing data
                'selection_neuron' : UTILS.selection_neuron_seuil,
                'how_to_define_batchsize' : None,
                }


MLP_model = TINY.TINY(dico_parameters)

TINY.X_train_rescale = torch.randn(500, 1, 1, MLP_model.init_X_shape[-1])
Y_train_true = torch.concatenate([torch.sin(TINY.X_train_rescale[:, 0, 0, :].sum(dim = -1, keepdim = True)*k) for k in range(1, 11)], dim = 1)
TINY.Y_train_rescale  = torch.nn.functional.one_hot(Y_train_true.argmax(1)).float()

TINY.X_test_rescale = torch.randn(50, 1, 1,  MLP_model.init_X_shape[-1])
Y_test_true = torch.concatenate([torch.sin(TINY.X_test_rescale[:, 0, 0, :].sum(dim = -1, keepdim = True)*k) for k in range(1, 11)], dim = 1)
TINY.Y_test_rescale = torch.nn.functional.one_hot(Y_test_true.argmax(1)).float()
MLP_model.how_to_define_batchsize = define_Batchsize




