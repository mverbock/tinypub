import torch

config = '2C2L'
num_expe = 3


size_batch_estimation = 50000
name_file_expe = 'results/' + config + '/expe_' + str(num_expe) + '/' 
print('path expe :', name_file_expe)




if config == '1C5L' :

    layer_name = { 1 : 'CB', 2 : 'L', 3 : 'L', 4 : 'L', 5 :'L', 6 : 'L'}

    skeleton = {0  : {},
                1 : {'in_channels' : 3, 'out_channels' : 1, 'kernel_size' : (3, 3), 'padding' : (1, 1), 'stride' : (1, 1)},
                2 : {'size' : 1},
                3 : {'size' : 1},
                4 : {'size' : 1},
                5: {'size' : 1},
                6 : {'size' : 10}}


    fct = {depth : torch.nn.SELU() for depth in range(1, 6)}
    fct.update({6 : torch.nn.Identity()})
    #fct.update({2 : MaxPoolSELU()})
    #fct.update({4 : MaxPoolSELU()})
    to_add = [depth for depth in range(1, 6)] # depths where neurons are added
    to_add_C = [d for d in to_add if not(layer_name[d][0] == 'L')] # depths where neurons are added and are convolutional layer
    
elif config == '2C2L' :
    layer_name = { 1 : 'CB', 2 : 'CB', 3 : 'L', 4 : 'L'}

    skeleton = {0  : {},
                1 : {'in_channels' : 3, 'out_channels' : 1, 'kernel_size' : (3, 3), 'padding' : (1, 1), 'stride' : (1, 1)},
                2 : {'in_channels' : 1, 'out_channels' : 1, 'kernel_size' : (3, 3), 'padding' : (1, 1), 'stride' : (1, 1)},
                3 : {'size' : 1},
                4 : {'size' : 10}}


    fct = {depth : torch.nn.SELU() for depth in range(1, 4)}
    fct.update({4 : torch.nn.Identity()})
    to_add = [depth for depth in range(1, 4)] # depths where neurons are added
    to_add_C = [d for d in to_add if not(layer_name[d][0] == 'L')] # depths where neurons are added and are convolutional layer

elif config == '5C1L' :
    layer_name = { 1 : 'CB', 2 : 'CB', 3 : 'CB', 4 : 'CB', 5 :'CB', 6 : 'L'}

    skeleton = {0  : {},
                1 : {'in_channels' : 3, 'out_channels' : 1, 'kernel_size' : (3, 3), 'padding' : (1, 1), 'stride' : (1, 1)},
                2 : {'in_channels' : 1, 'out_channels' : 1, 'kernel_size' : (3, 3), 'padding' : (1, 1), 'stride' : (1, 1)},
                3 : {'in_channels' : 1, 'out_channels' : 1, 'kernel_size' : (3, 3), 'padding' : (1, 1), 'stride' : (1, 1)},
                4 : {'in_channels' : 1, 'out_channels' : 1, 'kernel_size' : (3, 3), 'padding' : (1, 1), 'stride' : (1, 1)},
                5 : {'in_channels' : 1, 'out_channels' : 1, 'kernel_size' : (3, 3), 'padding' : (1, 1), 'stride' : (1, 1)},
                6 : {'size' : 10}}


    fct = {depth : torch.nn.SELU() for depth in range(1, 6)}
    fct.update({6 : torch.nn.Identity()})
    to_add = [depth for depth in range(1, 6)] # depths where neurons are added
    to_add_C = [d for d in to_add if not(layer_name[d][0] == 'L')] # depths where neurons are added and are convolutional layer

    
    
    
    
depth_seuil = {depth : 10 for depth in range(1, 7)} # maximum number of neurons to add per layer
architecture_growth = 'Our'

batch_size = 32 # starting batchsize for training