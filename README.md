# TINYpub

## Description
Implementation of the architecture growth method "TINY" (https://openreview.net/forum?id=hbtG6s6e7r) on three different type of neural network architectures :
* Linear layer (MLP)
* Convolutional layer (CL)
* ResNet block (ResNet)


## Installation
The dependancies and theirs versions are in pyproject.toml. 
You can either execute this file or install them by hand checking the version with pyproject.toml.

To execute pyproject.toml, create a conda environnement, install the module poetry then run the command :
```
poetry install
```
### Working with CPUs only
Dowmload a version of torch and torchvision
```
conda install pytorch==2.2.0 torchvision==0.17.0 -c pytorch
```
### Working with GPU(s) :
Dowmload a version of torch and torchvision
```
# CUDA 11.8
conda install pytorch==2.2.0 torchvision==0.17.0 pytorch-cuda=11.8 -c pytorch -c nvidia
# CUDA 12.1
conda install pytorch==2.2.0 torchvision==0.17.0 pytorch-cuda=12.1 -c pytorch -c nvidia
```

## How to cite
```
@article{verbockhavenGrowingTinyNetworks2024b,
  title = {Growing {{Tiny Networks}}: {{Spotting Expressivity Bottlenecks}} and {{Fixing Them Optimally}}},
  shorttitle = {Growing {{Tiny Networks}}},
  author = {Verbockhaven, Manon and Rudkiewicz, Th{\'e}o and Charpiat, Guillaume and Chevallier, Sylvain},
  year = {2024},
  month = jul,
  journal = {Transactions on Machine Learning Research},
  issn = {2835-8856},
  langid = {english}
}
```


## License
GNU General Public License v3 (GPLv3)
