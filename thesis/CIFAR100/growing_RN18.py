import sys
sys.path.append('../../TINYpub/TINY/')
from mes_imports import *

import TINY as TINY
import UTILS
import SOLVE_EB as EB

import load_data_Loader
#sys.path.append('/home/tau/mverbock/TINYpub/TINY')

from settings import starting_architecture_RN18_TINY as starting_architecture
import GLOBALS as G

A_tr, A_te, L_tr, L_te, T = np.array([]), np.array([]), np.array([]), np.array([]), np.array([0])
df_tracker = pd.DataFrame()

def growing() :
    paths = G.paths
    global L_tr, L_te, A_tr, A_te, T, df_tracker, method


    def update_quantity_of_interest() :
        global L_tr, L_te, A_tr, A_te, T, df_tracker, method

        #global L_tr, L_te, A_tr, A_te, T, df_tracker
        dico_tracker = {'vps' + str(i) : [ RN.valeurs_propres[i].item()] for i in range(len(RN.valeurs_propres))}

        dico_tracker.update({'depth_add' : k, 'nbr_added_neuron': RN.nbr_added_neuron})
        dico_tracker.update({'accroissement' : [RN.accroissement.item()], 'portion_gain' : [RN.choosen_portion_gain.item()],
                        'nbr_parameters_apres' : [RN.count_parameters()], 'nbr_parameters_avant' : [nbr_parameters_avant],
                        'T' : [T[-1]], 'len_L_tr' : [len(L_tr)], 'lu_conv' : [RN.lu_conv], 'method' : [method]})

        df_tracker = pd.concat([df_tracker, pd.DataFrame.from_dict(dico_tracker)])

        L_tr = np.concatenate([L_tr, l_tr])
        L_te = np.concatenate([L_te, l_te])
        A_tr = np.concatenate([A_tr, a_tr])
        A_te = np.concatenate([A_te, a_te])
        T = np.concatenate([T, t +  T[-1]])

    def IncreaseBatchSize() :
        RN.batch_size = math.ceil(np.sqrt(RN.count_parameters() / nbr_parameters_avant) *  RN.batch_size)
        #RN.batch_size = math.ceil(RN.count_parameters() / nbr_parameters_avant *  RN.batch_size)
        
        
        
        
    for path in paths :
        A_tr, A_te, L_tr, L_te, T = np.array([]), np.array([]), np.array([]), np.array([]), np.array([0])
        df_tracker = pd.DataFrame()

        skip_connections = starting_architecture.skip_connections_RN18
        skip_fct = starting_architecture.skip_fct_RN18
        skeleton = starting_architecture.skeleton_RN18
        layer_name = starting_architecture.layer_name_RN18
        fct = starting_architecture.fct_RN18
        a, b, c, d = starting_architecture.a, starting_architecture.b, starting_architecture.c, starting_architecture.d
        a0, b0, c0, d0 = starting_architecture.a0, starting_architecture.b0, starting_architecture.c0, starting_architecture.d0
        ainf, binf, cinf, dinf = starting_architecture.ainf, starting_architecture.binf, starting_architecture.cinf, starting_architecture.dinf
        nbr_pass = starting_architecture.nbr_pass
        to_add = {2 : (ainf - a0), 4 : (ainf - a0), 7 : (binf - b0), 9 : (binf - b0), 
                  12 : (cinf - c0), 14 : (cinf - c0), 17 : (dinf - d0), 19 : (dinf - d0)}


        nbr_epochs_betw_adding = G.nbr_epochs_betw_adding
        architecture_growth = G.architecture_growth
        rescale = G.rescale
        batch_size = G.batch_size
        lambda_method = G.lambda_method
        lr = G.lr
        init_deplacement = G.init_deplacement
        init_deplacement_NG = G.init_deplacement_NG

        dico_parameters = {
                            'skeleton' : copy.deepcopy(skeleton),
                            'Loss' : UTILS.Loss_entropy,
                            'fct' : fct,
                            'layer_name' :layer_name,
                            'init_deplacement' : init_deplacement,
                            'init_deplacement_NG' : init_deplacement_NG,
                            'batch_size' : batch_size,
                            'lr' : lr,
                            'exp' : 2,
                            'lambda_method' : lambda_method,
                            'accroissement_decay' : 1e-7,
                            'accroissement_decay_NG' : 1e-7,
                            'lu_conv' : G.lu_conv,
                            'max_batch_estimation' : 100,
                            'max_amplitude' : G.max_amplitude,
                            'ind_lmbda_shape' : 1000,
                            'init_X_shape' : [3, 32, 32],
                            'skip_connections' : skip_connections,
                            'skip_fcts' : skip_fct,
                            'len_train_dataset' : 50000,
                            'len_test_dataset' : 10000,
                            'T_j_depth' : [d_add + 1 for d_add in to_add.keys()],
                            'take_random_part_of_M' : 1,
                            'selection_neuron' : UTILS.selection_neuron_seuil,
                            'selection_NG' : UTILS.selection_NG_Id,
                            'how_to_define_batchsize' : UTILS.indices_non_constant, 
                            'depth_seuil' : starting_architecture.depth_seuil,
                            'architecture_growth' : architecture_growth,
                            'rescale' : rescale
                            }
        RN = TINY.TINY(dico_parameters)

        RN.training_data, RN.test_data = load_data_Loader.load_database_CIFAR100(AugD=True, 
                                                                                 root = '../../TINYpub/DEMO/ResNet/data/')
        RN.tr_loader = DataLoader(RN.training_data, batch_size=RN.max_batch_estimation, shuffle=True)
        RN.te_loader = DataLoader(RN.test_data, batch_size=RN.max_batch_estimation, shuffle=True)

        for j in range(nbr_pass) :
            for k in list(to_add.keys())[:]:

                method = 'Add'
                nbr_parameters_avant = RN.count_parameters()
                #RN.tr_loader = DataLoader(RN.training_data, batch_size=RN.max_batch_estimation, shuffle=True)
                #RN.te_loader = DataLoader(RN.test_data, batch_size=RN.max_batch_estimation, shuffle=True)
                if RN.architecture_growth == 'Our' or (RN.architecture_growth == 'Random' and G.do_NG) :
                    RN.dico_w = None
                    RN.how_to_define_batchsize(RN, k + 1, method = 'NG')
                    EB.compute_optimal_update(RN, k + 1, update = False, compute_gain = False)
                RN.how_to_define_batchsize(RN, k, method = 'Add')
                EB.add_neurons(RN, k, update = True)
                while RN.beta_min == 0 and RN.architecture_growth == 'Random':
                    RN.how_to_define_batchsize(RN, k, method = 'Add')
                    EB.add_neurons(RN, k, update = True)

                to_add[k] -= RN.alpha.shape[0]
                if not(G.TailleBatchFixe) :
                    IncreaseBatchSize()

                optimizer = torch.optim.SGD(RN.parameters(), lr = RN.lr)
                RN.tr_loader = iter(UTILS.cycle(DataLoader(RN.training_data, batch_size=RN.batch_size, shuffle=True)))
                RN.te_loader = iter(UTILS.cycle(DataLoader(RN.test_data, batch_size=RN.batch_size, shuffle=True)))
                l_tr, l_te, l_va, a_tr, a_te, a_va, t = RN.train_batch(nbr_epochs_betw_adding, optimizer = optimizer)
                update_quantity_of_interest()
                del RN.tr_loader, RN.te_loader

                if G.do_NG :
                    method = 'NG'
                    print('Avant transform dico_w :', RN.dico_w['weight'].shape)
                    RN.transform_dico_w(k)
                    #RN.dico_w = None
                    print('Apres transform dico_w :', RN.dico_w['weight'].shape)

                    RN.how_to_define_batchsize(RN, k + 1, method = 'NG')
                    EB.compute_optimal_update(RN, k + 1, update = True)

                    #### training loop ####
                    #RN.tr_loader = DataLoader(RN.training_data, batch_size=RN.batch_size, shuffle=True)
                    #RN.te_loader = DataLoader(RN.test_data, batch_size=RN.batch_size, shuffle=True)
                    optimizer = torch.optim.SGD(RN.parameters(), lr = RN.lr)
                    RN.tr_loader = iter(UTILS.cycle(DataLoader(RN.training_data, batch_size=RN.batch_size, shuffle=True)))
                    RN.te_loader = iter(UTILS.cycle(DataLoader(RN.test_data, batch_size=RN.batch_size, shuffle=True)))
                    l_tr, l_te, l_va, a_tr, a_te, a_va, t = RN.train_batch(nbr_epochs_betw_adding, optimizer = optimizer)
                    update_quantity_of_interest()
                    del RN.tr_loader, RN.te_loader

            df_performance = pd.DataFrame.from_dict({'L_tr' : L_tr, 'L_te' : L_te, 'A_tr' : A_tr, 'A_te' : A_te, 'T' : T[1:]})
            df_tracker.to_csv(path  + '/df_tracker.csv')
            df_performance.to_csv(path  + '/df_performance.csv')
            RN.T = T[-1]
            RN.len_L_tr = len(L_tr)
            UTILS.save_model_to_file(RN, path = path + '/' , name='model_' + str((j + 1) * (k + 1)))


        ##############################################################
        ### Register #################################################
        ##############################################################    


        fig, axs = plt.subplots(1, 2, figsize = (10, 5))
        coef = 5
        axs[0].plot(UTILS.lisser_courbe(A_te, coef = coef), label= 'A_te')
        axs[0].plot(UTILS.lisser_courbe(A_tr, coef = coef), label = 'A_tr')
        axs[0].set_title('Acc')
        axs[0].legend()

        axs_param = axs[0].twinx()
        axs_param.plot(df_tracker['len_L_tr'] / coef, df_tracker['nbr_parameters_avant'], '+', color = 'red', label = 'nbr_params_av')
        axs_param.plot(df_tracker['len_L_tr'] / coef, df_tracker['nbr_parameters_apres'], '+', color = 'black', label = 'nbr_params_ap')
        axs[1].plot(UTILS.lisser_courbe(L_te, coef = coef), label= 'L_te')
        axs[1].plot(UTILS.lisser_courbe(L_tr, coef = coef), label = 'L_tr')
        axs[1].set_title('Loss')
        axs[1].legend()
        axs_param.legend()
        plt.savefig(path + '/Acc_Loss.png')
        plt.close()


        df_performance = pd.DataFrame.from_dict({'L_tr' : L_tr, 'L_te' : L_te, 'A_tr' : A_tr, 'A_te' : A_te, 'T' : T[1:]})
        df_tracker.to_csv(path  + '/df_tracker.csv')
        df_performance.to_csv(path + '/df_performance.csv')





