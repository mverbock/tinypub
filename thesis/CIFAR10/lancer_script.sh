#!/bin/bash

#SBATCH --output=log/launcher_%A.out

#SBATCH --error=log/launcher_%A.error

#SBATCH --output=output/run%A.out

#SBATCH --error=output/error_%A.error

#SBATCH --time=24-00:00:00

#SBATCH --nodelist republic-1



echo "Node List: " $SLURM_NODELIST
echo "jobID: " $SLURM_JOB_ID
echo "Partition: " $SLURM_JOB_PARTITION
echo "submit directory:" $SLURM_SUBMIT_DIR
echo "submit host:" $SLURM_SUBMIT_HOST
echo "In the directory: pwd"
echo "As the user: whoami"

source /home/tau/mverbock/.bashrc
eval "$(conda shell.bash hook)"
conda activate express

python script_batch.py