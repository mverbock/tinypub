import torchvision
import torch
import numpy as np
from torch.utils.data import DataLoader
from torchvision import datasets, transforms
import torchvision.transforms as tt
from torchvision.transforms import ToTensor, Normalize
from torchvision.datasets import ImageFolder

### ImageNet ###
def my_one_hot1000(y):
    b = torch.zeros(1000)
    b[y] = torch.ones(1)
    return (b)

def crop(Im) :
    size_x, size_y = Im.shape[1], Im.shape[2]
    start_crop_x, start_crop_y = None, None
    crop = 224
    if size_x - crop <= 0 :
        Im = torch.cat([Im, torch.zeros((Im.shape[0], crop - size_x, Im.shape[2]), device = Im.device)], dim = 1)
        start_crop_x = 0
    if size_y - crop <= 0 :
        Im = torch.cat([Im, torch.zeros((Im.shape[0], Im.shape[1], crop - size_y), device = Im.device)], dim = 2)
        start_crop_y = 0
    
    size_x, size_y = Im.shape[1], Im.shape[2]
    
    if  start_crop_x is None :
        start_crop_x = np.random.randint(size_x - crop)
    if start_crop_y is None :
        start_crop_y = np.random.randint(size_y - crop)
    
    return(Im[:, start_crop_x : start_crop_x + crop, start_crop_y : start_crop_y + crop])


### CIFAR-100 ###
def my_one_hot100(y):
    b = torch.zeros(100)
    b[y] = torch.ones(1)
    return (b)

### MNIST ###
def my_one_hot(y):
    b = torch.zeros(10)
    b[y] = torch.ones(1)
    return b


def data_loader(tr, te, batch_size=100):
    train_dataloader = DataLoader(tr, batch_size=batch_size, shuffle=True)
    test_dataloader = DataLoader(te, batch_size=batch_size, shuffle=True)
    return (train_dataloader, test_dataloader)




def load_database_ImageNet(AugD=True):
    global X_train_rescale
    global Y_train_rescale

    root_train = '/data/iceberg_1/titanic_4/datasets/Imagenet/train/'
    root_test = '/data/iceberg_1/titanic_4/datasets/Imagenet/val/'
    
    
    X_train_rescale = torch.zeros(0, 3, 256, 256)
    Y_train_rescale = torch.zeros(0, 1000)

    
    stats = ((0.4664, 0.4362, 0.3860), (0.2807, 0.2710, 0.2785))
    
    if AugD:
        train_tfms = tt.Compose([
                                 tt.RandomHorizontalFlip(),
                                 tt.ToTensor(),
                                 tt.Normalize(*stats, inplace=True),
                                 crop,
                                 #tt.RandomCrop(224, padding=4, padding_mode='reflect')
                                 ])

    else:
        train_tfms = tt.Compose([
            tt.ToTensor(),
            tt.Normalize(*stats, inplace=True),
            crop
        ])

    valid_tfms = tt.Compose([tt.ToTensor(), tt.Normalize(*stats)
                             ])
    
    training_data = ImageFolder(root= root_train, 
                                transform = train_tfms,
                                target_transform = my_one_hot1000)

    

    test_data =  ImageFolder(root= root_test,
                             transform = train_tfms,
                             target_transform = my_one_hot1000)
    
    return (training_data, test_data)




def load_database_CIFAR100(AugD=True, root = 'data'):
    global X_train_rescale
    global Y_train_rescale

    X_train_rescale = torch.zeros(0, 3, 32, 32)
    Y_train_rescale = torch.zeros(0, 100)

    # my_mean = torch.tensor([0.5068, 0.4864, 0.4407])
    # my_std = torch.tensor([0.2675, 0.2565, 0.2762])

    stats = ((0.5068, 0.4864, 0.4407), (0.2675, 0.2565, 0.2762))
    if AugD:
        train_tfms = tt.Compose([tt.RandomCrop(32, padding=4, padding_mode='reflect'),
                                 tt.RandomHorizontalFlip(),
                                 tt.ToTensor(),
                                 tt.Normalize(*stats, inplace=True)
                                 ])

    else:
        train_tfms = tt.Compose([
            tt.ToTensor(),
            tt.Normalize(*stats, inplace=True)
        ])

    valid_tfms = tt.Compose([tt.ToTensor(), tt.Normalize(*stats)
                             ])

    training_data = datasets.CIFAR100(
        root=root,
        train=True,
        download=True,
        transform=train_tfms,
        target_transform=my_one_hot100
    )

    test_data = datasets.CIFAR100(
        root=root,
        train=False,
        download=True,
        transform=valid_tfms,
        target_transform=my_one_hot100
    )

    return (training_data, test_data)

    

def load_database_CIFAR10(AugD = False, root = 'data') :
    global X_train_rescale 
    global Y_train_rescale

    X_train_rescale = torch.zeros(0, 3, 32, 32)
    Y_train_rescale = torch.zeros(0, 100)

    # my_mean = torch.tensor([0.5068, 0.4864, 0.4407])
    # my_std = torch.tensor([0.2675, 0.2565, 0.2762])

    stats = ((0.4914, 0.4822, 0.4465), (0.2470, 0.2435, 0.2616))
    if AugD:
        train_tfms = tt.Compose([tt.RandomCrop(32, padding=4, padding_mode='reflect'),
                                 tt.RandomHorizontalFlip(),
                                 tt.ToTensor(),
                                 tt.Normalize(*stats, inplace=True)
                                 ])

    else:
        train_tfms = tt.Compose([
            tt.ToTensor(),
            tt.Normalize(*stats, inplace=True)
        ])

    valid_tfms = tt.Compose([tt.ToTensor(), tt.Normalize(*stats)
                             ])

    training_data = datasets.CIFAR10(
    root=root,
    train=True,
    download=True,
    transform= train_tfms,
    target_transform = my_one_hot
    )

    test_data = datasets.CIFAR10(
            root=root,
            train=False,
            download=True,
            transform= valid_tfms,
            target_transform = my_one_hot
        )

    
    return(training_data, test_data)
    



def load_database_MNIST(root = 'data'):
    my_mean = torch.tensor(0.1307)
    my_std = torch.tensor(0.3081)

    training_data = datasets.MNIST(
        root=root,
        train=True,
        download=True,
        transform=transforms.Compose([ToTensor(), Normalize(my_mean, my_std)]),
        target_transform=my_one_hot
    )

    test_data = datasets.MNIST(
        root= root,
        train=False,
        download=True,
        transform=transforms.Compose([ToTensor(), Normalize(my_mean, my_std)]),
        target_transform=my_one_hot
    )

    # return(training_data, test_data)
    return (training_data, test_data)
    train_dataloader = DataLoader(training_data, batch_size=batch_size, shuffle=True)
    test_dataloader = DataLoader(test_data, batch_size=batch_size, shuffle=True)

    return (training_data, test_data)
