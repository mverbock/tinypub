import os, sys
sys.path.append('/home/tau/mverbock/TINYpub/TINY/')
#sys.path.append('/home/tau/mverbock/TINYpub/TINY/')

from mes_imports import *

import GLOBALS as G
import UTILS
import TINY as TINY
import load_data_Loader

G.reduction = 64
path = 'results/Baseline/1over64/'

from settings import starting_architecture_RN18_TINY as starting_architecture

epochs = 250
for expe in range(1, 2) :

    A_tr, A_te, L_tr, L_te, T = np.array([]), np.array([]), np.array([]), np.array([]), np.array([0])

    skip_connections = starting_architecture.skip_connections_RN18
    skip_fct = starting_architecture.skip_fct_RN18
    skeleton = starting_architecture.skeleton_RN18
    layer_name = starting_architecture.layer_name_RN18
    fct = starting_architecture.fct_RN18
    a, b, c, d = starting_architecture.a, starting_architecture.b, starting_architecture.c, starting_architecture.d
    
    nbr_epochs_betw_adding = G.nbr_epochs_betw_adding
    architecture_growth = G.architecture_growth
    rescale = G.rescale
    batch_size = G.batch_size
    lambda_method = G.lambda_method
    lr = G.lr


    dico_parameters = {
                        'skeleton' : copy.deepcopy(skeleton),
                        'Loss' : UTILS.Loss_entropy,
                        'fct' : fct,
                        'layer_name' :layer_name,
                        'init_deplacement' : 1e-8,
                        'init_deplacement_NG' : 1e-8,
                        'batch_size' : batch_size,
                        'lr' : lr,
                        'exp' : 2,
                        'lambda_method' : lambda_method,
                        'accroissement_decay' : 1e-8,
                        'accroissement_decay_NG' : 1e-8,
                        'lu_conv' : 0.001,
                        'max_batch_estimation' : 100,
                        'max_amplitude' : 20.,
                        'ind_lmbda_shape' : 1000,
                        'init_X_shape' : [3, 32, 32],
                        'skip_connections' : skip_connections,
                        'skip_fcts' : skip_fct,
                        'len_train_dataset' : 50000,
                        'len_test_dataset' : 10000,
                        'T_j_depth' : [],
                        'take_random_part_of_M' : 1,
                        'selection_neuron' : UTILS.selection_neuron_seuil,
                        'selection_NG' : UTILS.selection_NG_Id,
                        'how_to_define_batchsize' : UTILS.indices_non_constant, 
                        'depth_seuil' : starting_architecture.depth_seuil,
                        'architecture_growth' : architecture_growth,
                        'rescale' : rescale
                        }
    RN = TINY.TINY(dico_parameters)

    RN.training_data, RN.test_data = load_data_Loader.load_database_CIFAR100(AugD=True, 
                                                                            root='../../TINYpub/DEMO/ResNet/data/')
#    RN.tr_loader = iter(UTILS.cycle(DataLoader(RN.training_data, batch_size=RN.batch_size, shuffle=True)))
#    RN.te_loader = iter(UTILS.cycle(DataLoader(RN.test_data, batch_size=RN.batch_size, shuffle=True)))
    
    RN.tr_loader = iter(UTILS.cycle(DataLoader(RN.training_data, batch_size=RN.batch_size, shuffle=True)))
    RN.te_loader = iter(UTILS.cycle(DataLoader(RN.test_data, batch_size=RN.batch_size, shuffle=True)))
        
    optimizer = torch.optim.SGD(RN.parameters(), lr = RN.lr)

    l_tr, l_te, _, a_tr, a_te, _, t = RN.train_batch(epochs, optimizer = optimizer)

    L_tr, L_te = np.concatenate([L_tr, l_tr]), np.concatenate([L_te, l_te])
    A_tr, A_te = np.concatenate([A_tr, a_tr]), np.concatenate([A_te, a_te])
    T = np.concatenate([T, t])

    
    if not(str(expe) in os.listdir(path)) :
        os.mkdir(path + str(expe))
    df = pd.DataFrame.from_dict({'L_Tr' : L_tr, 'L_te' : L_te, 'A_tr' : A_tr, 'A_te' : A_te, 'T' : T[1:]})
    
    if not str(epochs) + '_epochs' in os.listdir(path + str(expe) ) :
        os.mkdir(path + str(expe) + '/' + str(epochs) + '_epochs/')
    df.to_csv(path + str(expe) + '/' + str(epochs) + '_epochs/df_performance.csv')
    del RN.tr_loader, RN.te_loader
    UTILS.save_model_to_file(RN, path + str(expe) + '/' +  str(epochs) +'_epochs/')