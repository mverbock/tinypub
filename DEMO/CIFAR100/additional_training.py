import time, gc, sys, math, copy, os

import torch
import torchvision
import torchvision.transforms as transforms
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from torch import nn, optim
from torch.utils.data import DataLoader
from tqdm.notebook import tqdm
print('current directory :', os.getcwd())
sys.path.append('../../TINY/')
        
from UTILS import *

import TINY as TINY
import SOLVE_EB as EB
import GLOBALS as G
import load_data_Loader

import re, os
print('cuda device :', torch.cuda.is_available())


epochs_supp = 250

paths = ['results/GradMax/1soixantequatre_tbadd_0.25/1/']



for path in paths :
    model_names = [p for p in os.listdir(path) if 'model' in p]
    model_number = [int(model_name[6:]) for model_name in model_names]
    name_model_final = 'model_' + str(sorted(model_number)[-1])
    model_final = read_model_from_file(path, name = name_model_final)
    print('path :', path)
    if not('entrainement_supplementaire' in os.listdir(path)) :
        
        os.mkdir(path + 'entrainement_supplementaire')
    if not(str(epochs_supp) + '_epochs' in os.listdir(path + 'entrainement_supplementaire')) :
        os.mkdir(path + 'entrainement_supplementaire/' + str(epochs_supp) + '_epochs')       
    path_writing = path + 'entrainement_supplementaire/' + str(epochs_supp) + '_epochs/'
    #model_final.tr_loader = iter(cycle(DataLoader(model_final.training_data, batch_size=model_final.batch_size, shuffle=True)))
    #model_final.te_loader = iter(cycle(DataLoader(model_final.test_data, batch_size=model_final.batch_size, shuffle=True)))
    model_final.tr_loader = iter(cycle(DataLoader(model_final.training_data, batch_size=model_final.batch_size, shuffle=True)))
    model_final.te_loader = iter(cycle(DataLoader(model_final.test_data, batch_size=model_final.batch_size, shuffle=True)))
                                
    optimizer = torch.optim.SGD(model_final.parameters(), lr = model_final.lr)
    l_tr, l_te, l_va, a_tr, a_te, a_va, T = model_final.train_batch(epochs_supp, optimizer = optimizer)
    df_performance_bis = pd.DataFrame.from_dict({'L_tr' : l_tr, 'L_te' : l_te, 'A_tr' : a_tr, 'A_te' : a_te})
    df_performance_bis.to_csv(path_writing + 'df_performance_' + str(sorted(model_number)[-1]) + '.csv')
    
    del model_final.tr_loader, model_final.te_loader 
    
    save_model_to_file(model_final, path = path_writing, name = name_model_final + '_' + str(epochs_supp) + '_epochs')
    plt.plot(df_performance_bis['A_tr'], label = 'A_tr')
    plt.plot(df_performance_bis['A_te'], label = 'A_te')
    plt.legend()
    plt.savefig(path_writing + 'Acc_final_' + str(epochs_supp) + '.png')
    plt.close()